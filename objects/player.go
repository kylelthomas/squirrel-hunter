package objects

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"image/color"
)

type Plr struct {
	X1 float64
	Y1 float64
	X2 float64
	Y2 float64
}

func (l *Plr) Init() Plr {
	l.X1 = 50
	l.Y1 = 100
	l.X2 = 50
	l.Y2 = 110
	return *l
}

func (l *Plr) MoveRight() {
	if l.X1 > 100 {
		l.X1 = 99
	}
	l.X1++
	if l.X2 > 100 {
		l.X2 = 99
	}
	l.X2++
}

func (l *Plr) MoveLeft() {
	if l.X1 == 1 {
		l.X1 = 2
	}
	l.X1--
	if l.X2 == 1 {
		l.X2 = 2
	}
	l.X2--
}

func (l *Plr) MoveUp() {
	if l.Y1 <= 0 {
		l.Y1 = 230
		l.Y2 = 240
	}
	l.Y1 = l.Y1 - 2
	l.Y2 = l.Y2 - 2
}

func (l *Plr) MoveDown() {
	if l.Y2 >= 240 {
		l.Y1 = 0
		l.Y2 = 10
	}
	l.Y1 = l.Y1 + 2
	l.Y2 = l.Y2 + 2
}

func (l *Plr) Draw(screen *ebiten.Image) {
	ebitenutil.DrawLine(screen, l.X1, l.Y1, l.X2, l.Y2, color.NRGBA{255, 255, 255, 255})
	ebitenutil.DrawLine(screen, l.X1+2, l.Y1+5, l.X2+5, l.Y1+5, color.NRGBA{255, 255, 255, 255})
}
